{-# LANGUAGE RebindableSyntax  #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
module Example.CameraControl where

import Lola
import Syntax.HLPrelude
import DecDyn
import Syntax.Booleans
import Lib.Utils
import Theories.Geometry2D
import Data.Maybe
import GHC.Generics
import Data.Aeson
import qualified Prelude as P
import Lib.UAVState

data Position = Position {x :: Double, y :: Double, alt :: Double, zone :: Double} deriving (Show,Generic,Read,FromJSON,ToJSON)
data Wind = Wind {speed_z :: Double, speed :: Double, direction :: Double} deriving (Show,Generic,Read,FromJSON,ToJSON)

data OutEvent = TakePicture | Skip deriving (Show,Generic,Read,FromJSON,ToJSON,Eq)

data PictureState = TakingPictures | NotTakingPictures deriving Eq

spec :: Specification
spec = [out outev]

evs_within :: Stream [String]
evs_within = Input "events_within"

wp_reached :: Stream Bool
wp_reached = Input "wp_reached"

picdistance :: Stream Double
picdistance = Input "picdistance"

uavst = (evs_within, wp_reached, flyingstate uavst)

position :: Stream Position
position = Input "position"
location :: Stream Point2
location = "location" =: pos2point <$> Now position
  where pos2point (Position x y _ _) = P x y

picturestate :: Stream PictureState
picturestate = "picturestate" =:
  if Now (flyingstate uavst) === Leaf GoingToSecondPoint then Leaf TakingPictures
  else Leaf NotTakingPictures

outev :: Stream OutEvent
outev = "outev" =:
  if followingline uavst
     || (Now picturestate === Leaf TakingPictures 
         && far <$> Now lastpictaken <*> Now location <*> Now picdistance)
  then Leaf TakePicture
  else Leaf Skip
  where far x y d = distance x y P.> d

lastpictaken :: Stream Point2
lastpictaken = "lastpictaken" =:
  if outev :@ (-1, Leaf Skip) === Leaf Skip then lastpictaken :@ (-1, Leaf (P 0 0))
  else location :@ (-1, Leaf undefined)
