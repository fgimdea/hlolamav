# Instructions to Compile and Run the Monitors
This README file briefly explains the steps necessary to compile and run the monitors shown in the paper "Mission Development by Combining Temporal Task Planning and Runtime Verification".

## System Requirements
To compile the monitors, you will need
* Go version 1.16 or newer
* Python version 3.7 or newer
* Stack version 2.7.3 or newer

## HLola Source Code
This zip contains the source code of the tool HLola[^1][^2] along with the specifications of the monitors mentioned in the paper, in the folder `monitors`.

### Compile the Compiler
To compile a monitor, you will first need to generate the compiler from source, using Go.
Navigate to the folder `frontend/hlolac/preproc` and run:
```
frontend/hlolac/preproc$ go build
```

### Compile a Monitor
To compile the monitor `cameracontrol.hlola`, navigate to the folder `monitors` and run the compiler using
```
monitors$ python3 ../frontend/hlolac/hlolac.py -o cameracontrol cameracontrol.hlola
```
This will generate a binary `cameracontrol` in the working directory (`monitors`).
The rest of the monitors can be compiled analogously.
Bear in mind that the first time a monitor is compiled, the Stack compiler will set up a local Haskell environment and will download all the necessary dependencies, which may take several minutes.
Successive compilations of the same or other monitors will be considerably faster.

### Run a Monitor
The monitors take input streams in JSON format through the standard input, and produce output streams in JSON format through the standard output.
This eases their usage from an external program, as is the hybrid controller in our setup.

## List of Monitors
We provide nine specifications, which we have used successfully in the empirical evaluation of the work in the paper.

### Back and Forth (No Decomposition)
The file `backforth.hlola` contains the specification in charge of implementing the "Back and Forth" strategy with a single region.

### Back and Forth (Camera Control)
The specification in `cameracontrol.hlola` is in charge of taking pictures when the UAV has moved far enaough and is in the corresponding state.

### Back and Forth (Exact Decomposition)
The monitor of the specification `multipolys.hlola` implements the additional monitor for the "Back and Forth" strategy with exact cellular decomposition, necessary to deal with multiple regions.

### Edge Counting, PatrolGRAPH* and LRTA*
The specification in `discretecover.hlola` implements the coverage of a matrix ensuring that every node is visited at least `minvisits` times before the mission is over, using one of the strategies "Edge Counting", "Node Counting" or "Learning Real Time A*".
You can modify the constants `minvisits` and `go` to try different behaviors.

### Recursive Non-Uniform Coverage
The specification in `recursivecoverage.hlola` implements the coverage of a terain capturing pictures at high altitudes and only going lower for refinement on those regions in which something interesting may be present.
It implements two strategies: `DFS` search over the underlying tree of regions, and a `Heuristic` search to avoid going up if a region has been covered and something interesting was being detected, under the assumption that it is likely that something interesting will be found in the area nearby.
You can switch between these two strategies modifying the variable `mode`.

### Wavefront Algorithm
The monitor of the specification `wavefront.hlola` iterates over a previously given list of locations every time a new instruction is required, implementing the Wavefront algorithm.

### Concrete Example - Monitor 1 (Sense)
The monitor of the specification in `sense.hlola` computes if a person has been detected and reports the result when queried.

### Concrete Example - Monitor 2 (Stability Check)
The specification `stable.hlola` calculates if the UAV is stable.

### Concrete Example - Monitor 3 (Drop Behavior)
The file `drop.hlola` specifies the behavior of the UAV when it is requested to drop a packet.

[^1]: _["Declarative Stream Runtime Verification (hLola)"](https://doi.org/10.1007/978-3-030-64437-6_2)_. Martín Ceresa, Felipe Gorostiaga, César Sánchez.
[^2]: _["HLola: a Very Functional Tool for Extensible Stream Runtime Verification"](https://doi.org/10.1007/978-3-030-72013-1_18)_. Felipe Gorostiaga, César Sánchez.
