{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
module Theories.TreeCover where -- (makeTree, showTree) where

import Data.Array
import Data.List
import Data.Maybe
import Data.Aeson hiding (Array)
import GHC.Generics

data Interestingness = IDK | NotInteresting | Interesting deriving (Show,Generic,Read,FromJSON,ToJSON,Eq)
data FlightTree = Node Int (Interestingness, Array Coord FlightTree) | LeafFT Interestingness

data Mode = DFS | Heuristic deriving Eq

type Coord = (Int, Int)
type NodePath = [Coord]
type Continuation = Maybe NodePath

height :: FlightTree -> Int
height (LeafFT _) = 0
height (Node x _) = x

getIntness :: FlightTree -> Interestingness
getIntness (LeafFT x) = x
getIntness (Node _ (x,_)) = x

makeTree :: (Int, Int) -> FlightTree
makeTree (0, side) = LeafFT IDK
makeTree (height, side) = Node height (IDK, listArray ((0,0),(side-1,side-1)) (repeat $ makeTree ((height-1), side)))

showTree :: FlightTree -> String
showTree ft@(Node height (_, children)) = let
  (_,(x,_)) = bounds children
  size = (x+1)^height
  arr = showTree' ft (0,0) (listArray ((0,0),(size-1,size-1)) (repeat 'X'))
  lss = tolss elems arr
  in intercalate "\n" (map (intersperse ' ') lss)

showTree' :: FlightTree -> Coord -> Array Coord Char -> Array Coord Char
showTree' (LeafFT intness) offset arr = put arr (offset, printIntness intness)
showTree' (Node height (_, children)) (offsetx, offsety) arr = let
  (_,(x,_)) = bounds children
  multiplier = (x+1) ^ (height-1)
  ls = map (\((x,y), n) -> ((offsetx + multiplier*x,  offsety + multiplier*y), n)) (assocs children)
  in foldl (\ar (offs, n) -> showTree' n offs ar) arr ls

printIntness IDK = '?'
printIntness Interesting = 'X'
printIntness NotInteresting = 'O'

put arr arg@((ox,oy),_) = let
  (_,(x,_)) = bounds arr
  in if ox > x || oy > x then error ("Na: "++ show ox ++", " ++ show oy) else arr // [arg]

tolss f arr = let
  (_,(x,_)) = bounds arr
  in chunk (x+1) (f arr)

-- From https://stackoverflow.com/questions/44607813/better-way-to-write-this-function-list-to-matrix-list-of-lists
-- Unfolding:
chunk :: Int -> [a] -> [[a]]
chunk n = unfoldr (\xs -> if null xs then Nothing else Just (splitAt n xs))

nextNode :: Mode -> NodePath -> NodePath -> Continuation -> FlightTree -> (NodePath, Continuation)
nextNode mode position path continuation ft = let
  node = fromJust (getnode ft path)
  children = map (\(ix,_) -> path ++ [ix]) $ idkchildren path ft
  firstchild = listToMaybe ((filter (\neipath -> elem neipath children) (unvisitedneighs (take (length path+1) position) ft)) ++ children)
  parent = init path
  in
  if getIntness node == NotInteresting && isJust continuation then continue mode continuation position ft
  else
  maybe (if null path then ([], continuation) else
    if mode == DFS then nextNode mode position parent continuation ft
    else nextofnochildren mode position path continuation ft)
  (\fc -> (fc, continuation)) firstchild

nextofnochildren :: Mode -> NodePath -> NodePath -> Continuation -> FlightTree -> (NodePath, Continuation)
nextofnochildren mode position path continuation ft@(Node _ (_,arr)) = let
  -- (_,(x,_)) = bounds arr
  -- (absx,absy) = pathToAbsolute (x+1) path
  -- neighs = [(absx, absy-1), (absx-1, absy), (absx+1, absy), (absx, absy+1)]
  -- pathedneighs = map (absoluteToPath (length path - 1) (x+1)) neighs
  -- unvineighs = [x | x<-pathedneighs, let n = getnode ft x, isJust n, IDK==getIntness (fromJust n)]
  unvineighs = unvisitedneighs path ft
  mneigh = listToMaybe unvineighs
  parent = init path
  justnext = nextNode mode position parent continuation ft
  newcontinuation = maybe (Just (init path)) Just continuation
  -- in justnext -- This is for DFS
  in if length path < height ft && isJust continuation then continue mode continuation position ft
  else
  if length path < height ft || isNothing mneigh || not (null (idkchildren parent ft))
  then justnext else (fromJust mneigh, newcontinuation)

continue mode continuation position ft = nextNode mode position (fromJust continuation) Nothing ft

unvisitedneighs :: NodePath -> FlightTree -> [NodePath]
unvisitedneighs path ft@(Node _ (_,arr)) = let
  (_,(x,_)) = bounds arr
  (absx,absy) = pathToAbsolute (x+1) path
  neighs = [(absx, absy-1), (absx, absy+1), (absx-1, absy), (absx+1, absy)]
  pathedneighs = map (absoluteToPath (length path - 1) (x+1)) neighs
  in [x | x<-pathedneighs, let n = getnode ft x, isJust n, IDK==getIntness (fromJust n)]

idkchildren :: NodePath -> FlightTree -> [(Coord, FlightTree)]
idkchildren path ft = let
  node = fromJust (getnode ft path)
  -- children = getchildren node
  -- ordered = everyothereven reverse children
  in filter isidk $ concat (getchildren node)
  where
  isidk ((_, LeafFT intness)) = intness == IDK
  isidk ((_, Node _ (intness, _))) = intness == IDK

pathToAbsolute :: Int -> NodePath -> Coord
pathToAbsolute size nix = pathToAbs nix (0,0)
  where
  pathToAbs [] offset = offset
  pathToAbs ((px,py):r) (ox,oy) = pathToAbs r (size*ox+px,size*oy+py)

absoluteToPath :: Int -> Int -> Coord -> NodePath
absoluteToPath (-1) _ _ = []
absoluteToPath 0 _ coord = [coord]
absoluteToPath depth size (px, py) = let
  divi = size^depth
  in
  (px `div` divi, py `div` divi) : absoluteToPath (depth-1) size (px `mod` divi,py `mod` divi)

everyotherodd :: (a->a) -> [a] -> [a]
everyotherodd _ [] = []
everyotherodd f (x:rest) = f x:everyothereven f rest

everyothereven :: (a->a) -> [a] -> [a]
everyothereven _ [] = []
everyothereven f (x:rest) = x:everyotherodd f rest

getchildren :: FlightTree -> [[(Coord, FlightTree)]]
getchildren (LeafFT _) = []
getchildren (Node _ (_,arr)) = tolss assocs arr

getnode :: FlightTree -> NodePath -> Maybe FlightTree
getnode n [] = Just n
getnode (Node _ (_, arr)) (p@(x,y):r) = let
  ((lx, ly),(hx, hy)) = bounds arr
  validix l h  x = l <= x && x <= h
  in if validix lx hx x && validix ly hy y then getnode (arr ! p) r else Nothing

------------------------------
traverse :: FlightTree -> [(NodePath, FlightTree)]
traverse ft = traverse' ft Nothing [] (cycle (replicate 20 Interesting ++ [NotInteresting, NotInteresting]))
  where
  traverse' ft continuation p (lab:r) = let
    (nextix, continuation') = nextNode DFS p p continuation ft
    ft' = updateat nextix ft lab
    in
    if null nextix then [] else
    (nextix, ft') : traverse' ft' continuation' nextix r

updateat :: NodePath -> FlightTree -> Interestingness -> FlightTree
updateat [ix] (Node h (x,arr)) lab = Node h (x, arr // [(ix, updatenode lab (arr!ix))])
  where
  updatenode lab (Node h (_,arr)) = Node h (lab,if lab /= NotInteresting then arr else arr // [(ix, updatenode lab (arr!ix)) | ix <- indices arr])
  updatenode lab (LeafFT _) = LeafFT lab
updateat (ix:r) (Node h (x,arr)) lab = Node h (x, arr // [(ix, updateat r (arr!ix) lab)])
