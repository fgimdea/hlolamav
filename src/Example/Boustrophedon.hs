{-# LANGUAGE RebindableSyntax  #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
module Example.Boustrophedon where

import Lola
import Syntax.HLPrelude
import DecDyn
import Syntax.Booleans
import Lib.Utils
import Theories.Geometry2D
import Data.Maybe
import GHC.Generics
import Data.Aeson
import qualified Prelude as P
import Lib.UAVState

data Position = Position {x :: Double, y :: Double, alt :: Double, zone :: Double} deriving (Show,Generic,Read,FromJSON,ToJSON)
data Wind = Wind {speed_z :: Double, speed :: Double, direction :: Double} deriving (Show,Generic,Read,FromJSON,ToJSON)

data OutEvent = Skip | Goto (Point2, Double) | FinishedLine | FinishedPoly deriving (Show,Generic,Read,FromJSON,ToJSON,Eq)

spec :: Specification
spec = [out workingpoly, out windline, out outev, out location]

evs_within :: Stream [String]
evs_within = Input "events_within"

wp_reached :: Stream Bool
wp_reached = Input "wp_reached"

uavst = (evs_within, wp_reached, realstate)

poly :: Stream Polygon
poly = Input "poly"

position :: Stream Position
position = Input "position"
location :: Stream Point2
location = "location" =: pos2point <$> Now position
  where pos2point (Position x y _ _) = P x y

width :: Stream Double
width = Input "width"

wind :: Stream Wind
wind = Input "wind"
windline :: Stream Line
windline = "windline" =: toline.direction <$> Now wind
  where
  toline a
    | a P.> pi P./ 2 = toline (a-pi)
    | a P.<= -pi P./ 2 = toline (a+pi)
    | a == pi P./ 2 = Vert 0 -- Very unlikely but, who cares?
    | otherwise = Sloped (tan a) 0

realstate :: Stream FlyingState
realstate = "realstate" =: if Now outev === Leaf FinishedPoly then Leaf Idle else Now (flyingstate uavst)

outev :: Stream OutEvent
outev = "outev" =:
  if startline uavst then goto fst <$> Now edgesop
  else if followingline uavst then goto snd <$> Now edgesop
  else if linedone uavst then Leaf FinishedLine
  else Leaf Skip
  where
  goto _ Nothing = FinishedPoly -- only makes sense with fst
  goto f (Just (ps,_)) = Goto (f ps, getangle ps)

-- goto :: Stream (Maybe (Point2, Point2))
-- goto = "goto" =: fmap fst <$> Now edgesop

edgesop :: Stream (Maybe ((Point2, Point2), Point2))
edgesop = "edgesop" =: if startline uavst then
  sweepPerp <$> Now width <*> Now location <*> Now workingpoly <*> Now windline
  else edgesop :@ (-1, Leaf Nothing)

workingpoly :: Stream Polygon
workingpoly = "workingpoly" =: let
  prevpoly = workingpoly :@ (-1,Leaf [])
  slicedpoly = slicer <$> prevpoly in
  if Now poly /== Leaf [] then Now poly
  else if followingline uavst then maybe [] <$> slicedpoly <*> edgesop :@ (-1,Leaf undefined)
  else prevpoly
