{-# LANGUAGE RebindableSyntax  #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
module Example.PatrolGive.Mon1 where

import Lola
import Syntax.HLPrelude
import DecDyn
import Syntax.Booleans
import Syntax.Num
import Syntax.Ord
import Lib.Utils
import Theories.Geometry2D
import Data.Maybe
import Data.List
import Data.Array
import Data.Function
import GHC.Generics
import qualified Prelude as P
import Data.Aeson

data PersonAns = NA | Found | NotFound
  deriving (Show,Generic,Read,FromJSON,ToJSON,Eq)

spec :: Specification
spec = [out senseanswer]

evs_within :: Stream [String]
evs_within = Input "events_within"

persondetected :: Stream Bool
persondetected = Input "person_detected"

senseperson :: Stream Bool
senseperson = "senseperson" =: elem "sense.person" <$> Now evs_within

pdcounter :: Stream Int
pdcounter = "pdcounter" =:
  pdcounter :@ (-1,0)
  + (fromEnum <$> Now persondetected)
  - (fromEnum <$> persondetected :@ (-5, Leaf False))

personfound :: Stream Bool
personfound = "personfound" =: Now pdcounter >= 3

carrypf :: Stream Bool
carrypf = "carrypf" =:
  Now senseanswer === Leaf NA
  && (Now personfound || carrypf :@ (-1, Leaf False))

senseanswer :: Stream PersonAns
senseanswer = "senseanswer" =:
  if not (Now senseperson) then Leaf NA else
  if Now personfound || carrypf :@ (-1,Leaf False) then Leaf Found
  else Leaf NotFound
