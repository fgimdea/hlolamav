{-# LANGUAGE RebindableSyntax  #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
module Example.Iterator where

import Lola
import Syntax.HLPrelude
import DecDyn
import Syntax.Booleans
import Lib.Utils
import Theories.Geometry2D
import Data.Maybe
import Data.List
import Data.Array
import Data.Function
import GHC.Generics
import qualified Prelude as P
import Theories.CoverStrategies.Types

type Pos = (Int, Int)

spec :: Specification
spec = [out goto, out mission_finished]

evs_within :: Stream [String]
evs_within = Input "events_within"

inilist :: Stream [Pos]
inilist = Input "inilist"

reqmove :: Stream Bool
reqmove = "reqmove" =: elem "go.next" <$> Now evs_within

list :: Stream [Pos]
list = "list" =:
  if P.not.P.null <$> Now inilist then Now inilist
  else snd <$> retlist :@(-1, Leaf (undefined, []))

retlist :: Stream (Maybe Pos, [Pos])
retlist = "retlist" =:
  if Now reqmove then splitJust <$> Now list
  else ((,) Nothing) <$> Now list
  where
  splitJust (a:r) = (Just a, r)
  splitJust [] = (Nothing, [])

goto :: Stream (Maybe Pos)
goto = "goto" =: fst <$> Now retlist

mission_finished :: Stream Bool
mission_finished = "mission_finished" =:
  Now reqmove && (null <$> Now list) && not (Now mission_already_finished)

mission_already_finished :: Stream Bool
mission_already_finished = "maf" =:
  mission_already_finished :@ (-1,Leaf False) || 
  mission_finished :@ (-1,Leaf False)
