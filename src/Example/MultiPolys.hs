{-# LANGUAGE RebindableSyntax  #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
module Example.MultiPolys where

import Lola
import Syntax.HLPrelude
import DecDyn
import Syntax.Booleans
import Lib.Utils
import Theories.Geometry2D
import Data.Maybe
import Data.List
import GHC.Generics
import Data.Aeson
import qualified Prelude as P

data OutEvent = Skip | StartLine deriving (Show,Generic,Read,FromJSON,ToJSON,Eq)

spec :: Specification
spec = [out poly, out outev]

polys :: Stream [Polygon]
polys = Input "polys"

evs_within :: Stream [String]
evs_within = Input "events_within"

workingpolys :: Stream [Polygon]
workingpolys = "workingpolys" =: let
  prevpolys = workingpolys :@ (-1,Leaf []) in
  if P.not.null <$> Now polys then Now polys
  else prevpolys

startlineprefix = "start_line."

going_to_poly :: Stream Int
going_to_poly = "going_to_poly" =:
  if Now has_startline then getstartline <$> (Now evs_within) else going_to_poly :@ (-1, Leaf (-1))
  where
  getstartline ls = let
    ev = head.filter (isPrefixOf startlineprefix) $ ls
    in read.drop (length startlineprefix) $ ev

has_startline :: Stream Bool
has_startline = "has_startline" =: P.not.null.filter (isPrefixOf startlineprefix) <$> Now evs_within

outev :: Stream OutEvent
outev = "outev" =:
  if Now has_startline then Leaf StartLine else Leaf Skip

poly :: Stream Polygon
poly = "poly" =: let
  nowpoly = Now going_to_poly in
  if nowpoly === going_to_poly :@ (-1, Leaf (-1)) then Leaf []
  else (!!) <$> Now workingpolys <*> nowpoly
