{-# LANGUAGE RebindableSyntax  #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
module Example.PatrolGive.Mon2 where

import Lola
import Syntax.HLPrelude
import DecDyn
import Syntax.Booleans
import Syntax.Num
import Syntax.Ord
import Lib.Utils
import Theories.Geometry2D
import Data.Maybe
import Data.List
import Data.Array
import Data.Function
import GHC.Generics
import qualified Prelude as P

spec :: Specification
spec = [out uav_stable]

altitude :: Stream Double
altitude = Input "altitude"

angle :: Stream Double
angle = Input "angle"

stable :: Stream Double -> Expr Bool
stable str = let
  shift r x = x:(take 4 r)
  prev5 = "stable_prev5" <: str =: shift <$> prev5 :@ (-1, Leaf []) <*> Now str
  maxdif l = abs (minimum l - maximum l)
  in (maxdif <$> Now prev5) < 100

uav_stable :: Stream Bool
uav_stable = "uav_stable" =:
  stable angle && stable altitude
