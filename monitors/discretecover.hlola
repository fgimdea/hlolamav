format JSON
use theory CoverStrategies.Types
use qualified theory CoverStrategies.EdgeCounting as EdgeCounting
use qualified theory CoverStrategies.NodeCounting as NodeCounting
use qualified theory CoverStrategies.LRTA as LRTA
use theory Geometry2D
use haskell Data.Maybe
use haskell Data.List
use haskell Data.Array
use haskell Data.Function

input (Position,String) inimatrix

input Bool reqmove

input Position position

const go = EdgeCounting.go
-- const go = NodeCounting.go
-- const go = LRTA.go
const minvisits = 1

fun newMatrix ((0,_),_) = False
fun newMatrix _ = True

define Matrix matrix =
  if 1'newMatrix inimatrix[now]
  then 1'creatematrix inimatrix[now]
  else 1'snd retmatrix [-1|'(Nothing, dfltmatrix)]

-- go determines the strategy
define (Maybe Position, Matrix) retmatrix = let
  gotopos = 2'go position[now] matrix[now] in
  if reqmove[now] then 1'(mapFst Just) gotopos
  else 1'((,) Nothing) matrix[now]
  where
  mapFst f (a, b) = (f a, b)

output (Maybe Position) goto =
  1'fst retmatrix[now]

define Matrix countermatrix = let
  prevmatrix = countermatrix [-1| 'dfltmatrix]
  pos = 1'fromJust goto[now]
  val = 2'(!) prevmatrix pos
  in
  if 1'newMatrix inimatrix[now]
  then 1'creatematrix inimatrix[now]
  else if 1'isJust goto[now]
  then 3'update prevmatrix pos val
  else prevmatrix
  where
  update m p v = m // [(p,v+1)]

output Bool mission_finished =
  if 1'isNothing goto[now]
     || mission_already_finished[now]
  then 'False else
     1'checkDone countermatrix [-1| 'dfltmatrix]
  where
  done x = x == -1 P.|| x P.>= minvisits
  checkDone m = all done (elems m)

define Bool mission_already_finished =
  mission_already_finished [-1| 'False]
  || mission_finished [-1| 'False]
