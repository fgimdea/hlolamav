{-# LANGUAGE RebindableSyntax  #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
module Example.TreeCover where

import Lola
import Syntax.HLPrelude
import Syntax.Booleans
import Syntax.Num
import DecDyn
import qualified Prelude as P
import Theories.TreeCover

spec :: Mode -> Specification
spec mode = [out goto]
  where
  initree :: Stream (Int, Int)
  initree = Input "initree"

  interestingness :: Stream Interestingness
  interestingness = Input "interestingness"

  tree :: Stream FlightTree
  tree = "tree" =: let
    prevtree = tree :@ (-1, Leaf undefined)
    newtree = updateat <$> lastix <*> prevtree <*> Now interestingness
    in
    if (/=0).snd <$> Now initree then makeTree <$> Now initree
    else
    if reqmove && not (goingtoroot :@ (-1, Leaf True)) then newtree else prevtree

  size :: Stream Int
  size = "size" =: let
    msize = snd <$> Now initree
    in
    if msize /== 0 then msize else size :@ (-1,Leaf 0)

  goto :: Stream (Maybe (Int, Coord))
  goto = "goto" =: 
    if fstinstant then Leaf (Just (0, (0,0)))
    else
    fmap <$> (pathtocoord <$> Now size) <*> Now mstate
    where
    fstinstant = Now goingtoroot && not (goingtoroot :@ (-1, Leaf False))
    pathtocoord s (path, _) 
      | null path = (-1, (0,0))
      | otherwise = (length path, pathToAbsolute s path)

  mstate :: Stream (Maybe (NodePath, Continuation))
  mstate = "mstate" =:
    if reqmove then Just <$> (nextNode mode <$> lastix <*> lastix <*> lastcont <*> Now tree)
    else Leaf Nothing

  lastix = Now (lastproj "fst" fst [])
  lastcont = Now (lastproj "snd" snd Nothing)

  lastproj :: Streamable a => String -> ((NodePath, Continuation) -> a) -> a -> Stream a
  lastproj s f dflt = "lastproj" <: s =: maybe <$> lastproj s f dflt :@ (-1, Leaf dflt) <*> Leaf f <*> mstate :@ (-1, Leaf Nothing)

  reqmove = Now interestingness /== Leaf IDK

  goingtoroot :: Stream Bool
  goingtoroot = "goingtoroot" =:
    goingtoroot :@ (-1, Leaf True) && not (Now interestingness /== Leaf IDK)
