{-# LANGUAGE RebindableSyntax  #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
module Lib.UAVState where

import Lola
import Syntax.HLPrelude
import DecDyn
import Syntax.Booleans
import Lib.Utils
import Theories.Geometry2D
import Data.Maybe
import GHC.Generics
import Data.Aeson
import qualified Prelude as P

data FlyingState = Idle 
  | GoingToFirstPoint
  | GoingToSecondPoint
  deriving (Show,Eq)

data UAVEvent = StartLine | WaypointReached | NoEvent deriving (Show,Eq)

type UAVSt = (Stream [String], Stream Bool, Stream FlyingState)

startline :: UAVSt -> Expr Bool
startline uavst = newstate uavst === Leaf GoingToFirstPoint

followingline :: UAVSt -> Expr Bool
followingline uavst = newstate uavst === Leaf GoingToSecondPoint

linedone :: UAVSt -> Expr Bool
linedone uavst = 
  let fst = flyingstate uavst in
  Now fst === Leaf Idle && fst:@(-1,Leaf Idle) === Leaf GoingToSecondPoint

newstate :: UAVSt -> Expr FlyingState
newstate uavst = let
  fst = flyingstate uavst
  oldst = fst :@ (-1, Leaf Idle)
  newst = Now fst
  in if oldst /== newst then newst else Leaf Idle

flyingstate :: UAVSt -> Stream FlyingState
flyingstate uavst@(evs_within, wp_reached, realstate) = "flyingstate" =: let
  ev = extractEvent <$> Now evs_within <*> Now wp_reached in
  getNextState <$> realstate :@ (-1,Leaf Idle) <*> ev
  where
  extractEvent ls wpr
    | elem "start_line" ls = StartLine -- The order is important
    | wpr = WaypointReached
    | otherwise = NoEvent
  getNextState x NoEvent  = x
  getNextState Idle StartLine  = GoingToFirstPoint
  getNextState GoingToFirstPoint StartLine  = GoingToFirstPoint
  getNextState GoingToFirstPoint WaypointReached  = GoingToSecondPoint
  getNextState GoingToSecondPoint WaypointReached  = Idle
  getNextState GoingToSecondPoint StartLine  = GoingToFirstPoint
  getNextState x y  = error ("Invalid state update: at " ++ show x ++ " received " ++ show y)
