#!/bin/bash
CMDS=(\
  './generator.py mon1 | HLola patrolgive mon1'\
  './generator.py mon2 | HLola patrolgive mon2'\
  './generator.py mon3 | HLola patrolgive mon3'\
  './generator.py dfs | HLola tree DFS'\
  './generator.py boustrophedon | HLola boustrophedon'\
  './generator.py multipolys | HLola multipolys'\
  './generator.py discrete | HLola discretecover 5 lrta'\
  './generator.py discrete | HLola discretecover 5 edgecounting'\
  './generator.py iterator | HLola iterator'\
  )

for cmd in "${CMDS[@]}"; do
  echo "$cmd"
  ( timeout 10s bash -c "$cmd" || true ) | wc -l
done
rm HLola.{aux,hp}
