#!/bin/bash
CMDS=(\
  './generator.py mon1 | HLola patrolgive mon1'\
  './generator.py mon2 | HLola patrolgive mon2'\
  './generator.py mon3 | HLola patrolgive mon3'\
  './generator.py dfs | HLola tree DFS'\
  './generator.py boustrophedon | HLola boustrophedon'\
  './generator.py multipolys | HLola multipolys'\
  './generator.py iterator | HLola iterator'\
  )

CMDSTRICT=('./generator.py discrete | HLolaStrict discretecover 5 lrta'\
  './generator.py discrete | HLolaStrict discretecover 5 edgecounting')

COUNTER=0
for cmd in "${CMDS[@]}"; do
  let COUNTER++
  echo $COUNTER: "$cmd"
  timeout 10s bash -c "$cmd +RTS -hc -i0.1" > /dev/null
  hp2ps -c -m1 -s HLola.hp
  mv HLola.ps prof_$COUNTER.ps
done

COUNTER=0
for cmd in "${CMDSTRICT[@]}"; do
  let COUNTER++
  echo $COUNTER: "$cmd"
  timeout 10s bash -c "$cmd +RTS -hc -i0.1" > /dev/null
  hp2ps -c -m1 -s HLolaStrict.hp
  mv HLolaStrict.ps profstrict_$COUNTER.ps
done
