{-# LANGUAGE RebindableSyntax  #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
module Example.PatrolGive.Mon3 where

import Lola
import Syntax.HLPrelude
import DecDyn
import Syntax.Booleans
import Syntax.Num
import Syntax.Ord
import Lib.Utils
import Theories.Geometry2D
import Data.Maybe
import Data.List
import Data.Array
import Data.Function
import GHC.Generics
import qualified Prelude as P
import Data.Aeson

data Pos = Top | Bot
  deriving Eq

data State = Idle | Descending | Stabilizing Pos | DroppingPacket | Ascending
  deriving Eq

drop_height = 10
idle_height = 50

spec :: Specification
spec = [out cmd_droppacket, out cmd_resumeflight, out altitude_goto]

evs_within :: Stream [String]
evs_within = Input "events_within"

altitude :: Stream Double
altitude = Input "altitude"

uav_stable :: Stream Bool
uav_stable = Input "uav_stable"

dropcmd :: Stream Bool
dropcmd = "dropcmd" =: elem "drop.packet" <$> Now evs_within

state :: Stream State
state = "state" =: transition <$> state :@ (-1, Leaf Idle) <*> Now dropcmd <*> Now altitude <*> Now uav_stable
  where
  ifThenElse True x _ = x
  ifThenElse False _ x = x
  transition Idle True _ _ = Descending
  transition Descending _ alt _ = if alt P.<= drop_height then Stabilizing Bot else Descending
  transition (Stabilizing Bot) _ _ True = DroppingPacket
  transition DroppingPacket _ _ _ = Ascending
  transition Ascending _ alt _ = if alt P.>= idle_height then Stabilizing Top else Ascending
  transition (Stabilizing Top) _ _ True = Idle
  transition st _ _ _ = st

cmd_droppacket :: Stream Bool
cmd_droppacket = "cmd_droppacket" =: Now state === Leaf DroppingPacket

cmd_resumeflight :: Stream Bool
cmd_resumeflight = "cmd_resumeflight" =: Now state === Leaf Idle && state :@ (-1,Leaf Idle) /== Leaf Idle

altitude_goto :: Stream Double
altitude_goto = "altitude_goto" =:
  if Now state === Leaf Descending then Leaf drop_height
  else if Now state === Leaf Ascending then Leaf idle_height
  else Now altitude
