{-# LANGUAGE RebindableSyntax  #-}
module Main where
import InFromFile
import System.IO
import Lola
import System.Environment
import Prelude
import qualified Prelude as P
import DecDyn
import qualified Example.CameraControl (spec)
import qualified Example.Boustrophedon (spec)
import qualified Example.MultiPolys (spec)
import qualified Example.DiscreteCover (spec)
import qualified Example.Iterator (spec)
import qualified Theories.CoverStrategies.EdgeCounting as EdgeCounting (go)
import qualified Theories.CoverStrategies.NodeCounting as NodeCounting (go)
import qualified Theories.CoverStrategies.LRTA as LRTA (go)
import qualified Example.TreeCover (spec)
import qualified Theories.TreeCover (Mode(..))
import qualified Example.PatrolGive.Mon1 (spec)
import qualified Example.PatrolGive.Mon2 (spec)
import qualified Example.PatrolGive.Mon3 (spec)

main :: IO ()
main = getArgs >>= parseArgs

parseArgs :: [String] -> IO ()
parseArgs ls = do
  hSetBuffering stdin LineBuffering
  hSetBuffering stdout LineBuffering
  runSpecJSON False (specfromargs ls)

-- specfromargs [] = Example.Boustrophedon.spec
specfromargs ["tree", x] =
  Example.TreeCover.spec (mode x)
  where
    mode "DFS" = Theories.TreeCover.DFS
    mode "Heuristic" = Theories.TreeCover.Heuristic
specfromargs ["boustrophedon"] = Example.Boustrophedon.spec
specfromargs ["camera"] = Example.CameraControl.spec
specfromargs ["multipolys"] = Example.MultiPolys.spec
specfromargs ["discretecover", minvisits, strategy] = Example.DiscreteCover.spec (switch strategy) (read minvisits)
  where
    switch "edgecounting" = EdgeCounting.go
    switch "nodecounting" = NodeCounting.go
    switch "lrta" = LRTA.go
specfromargs ["iterator"] = Example.Iterator.spec
specfromargs ["patrolgive",x] = switch x
  where
    switch "mon1" = Example.PatrolGive.Mon1.spec
    switch "mon2" = Example.PatrolGive.Mon2.spec
    switch "mon3" = Example.PatrolGive.Mon3.spec
specfromargs _ = error "Wrong arguments. Check spec."
